from p5 import *

vitesse_x = 5   #définie la vitesse de la balle
vitesse_y = 2

vitesse_a = 0   #définie la vitesse des raquettes
vitesse_b = 4

vitesse_p = 2   #définie la vitesse du piège


x0=40   #définie les coordonées de base de la balle
y0=185

x_r1=25     #définie les coordonées de la raquette 1
y_r1=170

x_r2=575    #définie les coordonées de la raquette 2
y_r2=170


x_p=300     #définie les coordonées du piège
y_p=200

debut=0
compteur=0  #c'est la que les scores sont enregistré
compteur2=0

frameCount=0 # permet de compter les frames


def balle():    #définition des comportements de la balle
    global x0,y0,debut,vitesse_x,vitesse_y, x_r1, y_r1, x_r2, y_r2,compteur,compteur2

    if compteur<=3 and compteur2<=3:
        fill(255, 255,0)
        circle(x0, y0, 10)
    elif compteur<=5 and compteur2<=5:
        fill(255,255,255)
        circle(x0, y0, 10)
        triangle(x0, y0, x0-13,y0-13,x0+13,y0-13)
    else:
        circle(x0, y0, 10)

    if  key_is_pressed:

        if key=="u":    #définie le bouton pour faire commencer la balle a bouger
            debut = 1



    if debut==1:

        if x0+5 > 585 or x0-5 < 15:      #fait en sorte que quand la balle touche un côté de la zone de dessin, elle rebondie
            vitesse_x = -vitesse_x

        if y0+5 > 385 or y0-5 < 20:
            vitesse_y = -vitesse_y

        if x0-5==x_r1+5 and y_r1<=y0<=y_r1+60:      #fait en sorte que la balle rebondi sur la raquette 1
            vitesse_x = -vitesse_x

        if (x0-5==x_r2-5 or x0-5==x_r2+5)  and y_r2<=y0<=y_r2+60:   #fait en sorte que la balle rebondi sur la raquette 2
            vitesse_x = -vitesse_x

        if (x0==x_p or x0==x_p)  and y_p<=y0<=y_p+100:      #fait en sorte que la balle rebondi sur le piège
            vitesse_x = -vitesse_x



        x0=x0+vitesse_x
        y0=y0+vitesse_y



def raquette1():    #création de la raquette 1
    global x_r1,y_r1,compteur,compteur2
    fill(0, 0,0)
    rect(x_r1, y_r1,5,60)



def raquette2():    #création de la raquette 2
    global x_r2,y_r2,compteur,compteur2
    fill(255, 255,255)
    rect(x_r2, y_r2,5,60)



def piege():    #création du piège
    global x_p,y_p,vitesse_p,frameCount
    fill(100,100,0)
    rect(x_p, y_p,5,100)

    if y_p > 285 or y_p < 20:   #fait en sorte que le piège rebondi sur les bords du dessin
        vitesse_p = -vitesse_p

    if frameCount == 1000:   #accélere la vitesse du piège
        vitesse_p = 4

    if frameCount == 2000:  #accélere la vitesse du piège
        vitesse_p = 5

    y_p=y_p+vitesse_p

def mouvement():
    global compteur,compteur2,x0,y0,vitesse_x,vitesse_y,x_r1,y_r1, x_r2,y_r2

    if  key_is_pressed:    #permet de faire bouger les raquettes avec les touches a, q, p et m

            if key=="a":
                if y_r1>25:
                    y_r1-=5
                if compteur >= 8:  #donne un bonus de vitesse de déplacement a la raquette 1 si le score du joueur 2 et supérieur a 8
                    y_r1-=6

            if key=="q":
                if y_r1<325:
                    y_r1+=5
                if compteur >= 8:
                    y_r1+=6

            if key=="p":
                if y_r2>25:
                    y_r2-=5
                if compteur2 >= 8:   #donne un bonus de vitesse de déplacement a la raquette 2 si le score du joueur 1 et supérieur a 8
                    y_r2-=6

            if key=="m":
                if y_r2<325:
                    y_r2+=5
                if compteur2 >= 8:
                    y_r2+=6


def score():
    global compteur,compteur2
    if x0 == 15:    #ajoute 1 point au score du joueur 2
        compteur += 1
        fill(100,0,0)
    text(str(compteur),310, 40)

    if x0 == 585:   #ajoute 1 point au score du joueur 1
        compteur2 += 1
    text(str(compteur2),290, 40)

    if compteur2 == 10:     #arrête le jeu quand le compteur2 atteindra 10 point et écrit Le joueur 2 a gagner
        text("Le joueur 1 a gagné",200,150)
        noLoop()

    if compteur == 10:      #arrête le jeu quand le compteur atteindra 10 point et écrit Le joueur 1 a gagner
        text("Le joueur 2 a gagné",210,150)
        noLoop()




def setup():
    size(600, 400)     # création d'un caneva (zone de dessin) de dimmension 600 sur 400
    background(00,200,50)      # remplissage du fond
    fill(255, 255,0)           # couleur de remplissage des figures
    noStroke()                 # enlève les contours sur les figures


def decore():   #création de tout le décore
    global compteur,compteur2

    if compteur<=3 and compteur2<=3:
        fill(255,255,255)
        rect(15, 20, 570, 5)
        rect(15, 380, 575, 5)
        rect(200, 20, 5, 360)
        rect(400, 20, 5, 360)
        rect(15, 20, 5, 360)
        rect(585, 20, 5, 360)
        rect(200, 200, 200, 5)
        fill(255,100,100)
        rect(0,0,600,20)
        rect(0, 385,600,15)
        rect(0, 0, 15, 400)
        rect(590,0,15,400)

    elif compteur<=5 and compteur2<=5:
        fill(255,255,255)
        rect(15, 20, 570, 5)
        rect(15, 380, 575, 5)
        rect(200, 20, 5, 360)
        rect(400, 20, 5, 360)
        rect(15, 20, 5, 360)
        rect(585, 20, 5, 360)
        fill(150,255,0)
        rect(0,0,600,20)
        rect(0, 385,600,15)
        rect(0, 0, 15, 400)
        rect(590,0,15,400)
        fill(255,255,255)
        rect(400, 200, 185, 5)
        rect(15, 200, 190, 5)
        rect(15, 50, 575, 5)
        rect(15, 350, 575, 5)
        rect(45, 25, 5, 355)
        rect(555, 25, 5, 355)

    else:
        decore2()

def decore2():
    global compteur,compteur2,x_r1,x_r2
    #if compteur>=4 or compteur2>=2:
    x_r1=60
    x_r2=540
    fill(150,150,150)
    rect(0,0,600,400)
    fill(100,50,255)
    rect(15,15,570,370)
    fill(255,255,255)
    rect(15, 15, 570, 5)
    rect(15, 380, 575, 5)
    rect(15,200,570,5)
    rect(15, 20, 5, 360)
    rect(585, 15, 5, 365)

def draw():

    global compteur,compteur2,x0,y0,vitesse_x,vitesse_y,x_r1,y_r1, x_r2,y_r2,jeux

    if compteur<=3 and compteur2<=3:
        background(125,220,100)
    else:
        background(0,150,255)


    #decore()
    decore()
    balle()
    raquette1()
    raquette2()
    piege()
    mouvement()
    score()





run()
